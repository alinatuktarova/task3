"use strict";

document.querySelector('.user__button').addEventListener('click', async () => {
  render(await getResource('https://jsonplaceholder.typicode.com/users'));
});

async function getResource(url) {
  const response = await fetch(url);
  return await response.json();
}

function render(users) {
  let root = document.querySelector('.user__elements');
  root.innerHTML = '';
  users.forEach(({
    id,
    name
  }) => {
    let button = document.createElement('button');
    button.id = id;
    button.className = 'element';
    button.innerHTML = name;
    button.addEventListener('click', () => {
      renderList(id);
    });
    root.appendChild(button);
  });
  document.querySelector('.posts-choise__showbutton').style.display = 'block';
  document.querySelector('.posts-choise__select').style.display = 'block';
}

async function renderList(id) {
  let posts = await getResource('https://jsonplaceholder.typicode.com/posts');
  let select = document.querySelector('.posts-choise__select');
  select.options.length = 0;
  let elem_color = document.querySelectorAll('.element');

  for (let element of elem_color) {
    element.style.backgroundColor = 'transparent';
    element.style.color = 'black';
  }

  posts.forEach(({
    userId,
    title
  }) => {
    if (userId == id) {
      document.getElementById(id).style.backgroundColor = 'rgb(149, 149, 182)';
      document.getElementById(id).style.color = 'white';
      let option = document.createElement('option');
      option.text = title;
      option.value = title;
      select.add(option);
    }
  });
}

document.querySelector('.posts-choise__showbutton').addEventListener('click', async function renderPosts() {
  let posts = await getResource('https://jsonplaceholder.typicode.com/posts');
  const select = document.querySelector('.posts-choise__select');
  const div = document.querySelector('.posts-text');
  posts.forEach(({
    title,
    body
  }) => {
    if (title == select.value) div.innerHTML = body;
  });
});